# XXX: Replace with a canonical image when there is one
FROM registry.gitlab.com/juga0/pepdocker/peppythonadapter:latest as peppythonmixnet-test
COPY . peppythonmixnet
WORKDIR /peppythonmixnet
RUN python3 setup.py install

FROM peppythonmixnet-test as peppythonmixnet
RUN rm -rf /peppythonmixnet

FROM peppythonmixnet-test as postfix
ENV DEBIAN_FRONTEND noninteractive
RUN apt update -y; apt install -y postfix sudo syslog-ng
RUN apt autoclean

FROM postfix as node
ARG PORT=25
ARG DOMAIN=pep.example
ARG TRANSPORT=1
ARG MYNAMEADDR=""
ARG MYFP=""
ARG KEY=""
ARG USER=remailer
ENV PORT=${PORT}
ENV DOMAIN=${DOMAIN}
ENV MYNAMEADDR=${MYNAMEADDR}
ENV MYFP=${MYFP}
ENV KEY=${KEY}
ENV USER=${USER}
ENV TRANSPORT=${TRANSPORT}
# The user that will run postfix pipe
RUN if [ "$USER" != "root" ] ; then adduser --system ${USER}; fi
# Since pEp-2.1.0rc2, it doesn't use gnupg, therefore, there is no need to
# import the keys, but the client and remailer will need to be call with
# a key file to import a key
# RUN if [ -n "$KEY" ]; then sudo -u ${USER} gpg --import ${KEY}; fi
# To be able to se domain and port at runtime
ENTRYPOINT ["/peppythonmixnet/docker/configure_postfix.sh"]
CMD ["/peppythonmixnet/docker/start_postfix.sh"]
