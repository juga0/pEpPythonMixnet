#!/bin/bash

set -x

docker-compose up -d
docker exec alice client -d -p EB85BB5FA33A75E15E944E63F231550C4F47E38E -f /tests/data/plain_from_alice_to_bob.eml
docker exec bob cat /var/mail/root
echo "Congratulations, Bob has received the encrypted Email from Alice."
