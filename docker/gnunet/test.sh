#!/bin/bash

set -x

mkdir -p ./tmp/authority/.config
mkdir -p ./tmp/n1/.config
mkdir -p ./tmp/n2/.config
chown -R 1000:1000 ./tmp/authority ./tmp/n1 ./tmp/n2
docker-compose up -d gnunet.n1.pep.example gnunet.n2.pep.example authority.pep.example
sleep 3

echo "Test how would a node register itself in GNS via its GNUnet node and register to an Authority."
echo "In gnunet.n1.pep.example node, create an A record"
docker exec gnunet.n1.pep.example gnunet-identity -C root
docker exec gnunet.n1.pep.example gnunet-identity -d

echo "Check the identity and store it to use it in authority"
GNUNET1=$(docker exec gnunet.n1.pep.example gnunet-identity -d | grep root | awk '{print $3}')
echo $GNUNET1
echo "Set default identity for namestore subsystem"
docker exec gnunet.n1.pep.example gnunet-identity -s namestore -e root
echo "Check default identity"
docker exec gnunet.n1.pep.example gnunet-namestore -D
# docker exec gnunet.n1.pep.example gnunet-namestore -z root -a -e "1 d" -p -t A -n n1.pep.example -V 172.19.0.2
# Label `n1.pep.example' contains `.' which is not allowed
docker exec gnunet.n1.pep.example gnunet-namestore -z root -a -e "1 d" -p -t A -n n1 -V 172.19.0.2
docker exec gnunet.n1.pep.example gnunet-gns -t A -u n1.root

echo "In gnunet.n1.pep.example node, attempt to create a CERT record and fail.\n"
docker exec gnunet.n1.pep.example gnunet-namestore -z root -a -e "1 d" -p -t CERT -n n1 -V AAAA

echo "In gnunet.n1.pep.example node, create a TXT record"
docker exec gnunet.n1.pep.example gnunet-namestore -z root -a -e "1 d" -p -t TXT -n n1 -V email=root@n1.pep.example;layer=1;opengpg=AAAA

echo "In authority.pep.example node, create an A record"
docker exec authority.pep.example gnunet-identity -C root
docker exec authority.pep.example gnunet-identity -d
AUTHORITY=$(docker exec authority.pep.example gnunet-identity -d | grep root | awk '{print $3}')
echo $AUTHORITY
docker exec authority.pep.example gnunet-identity -s namestore -e root
docker exec authority.pep.example gnunet-namestore -D
docker exec authority.pep.example gnunet-namestore -z root -a -e "1 d" -p -t A -n authority -V 172.19.0.3
docker exec authority.pep.example gnunet-gns -t A -u authority.root

echo "In authority.pep.example node, delegate gnunet.n1.pep.example records"
docker exec authority.pep.example gnunet-namestore -z root -a -e "1 d" -p -t PKEY -n n1 -V $GNUNET1
echo "In authority, ask about n1 records."
docker exec authority.pep.example gnunet-gns -t A -u n1.n1.root

echo "In gnunet.n1.pep.example, delegate authority.pep.example records"
docker exec gnunet.n1.pep.example gnunet-namestore -z root -a -e "1 d" -p -t PKEY -n authority -V $AUTHORITY
echo "In gnunet.n1, ask about authority records."
docker exec gnunet.n1.pep.example gnunet-gns -t A -u authority.authority.root

echo "All process with gnunet.n2.pep.example"
docker exec gnunet.n2.pep.example gnunet-identity -C root
docker exec gnunet.n2.pep.example gnunet-identity -d
N2=$(docker exec gnunet.n2.pep.example gnunet-identity -d | grep root | awk '{print $3}')
echo $N2
docker exec gnunet.n2.pep.example gnunet-identity -s namestore -e root
docker exec gnunet.n2.pep.example gnunet-namestore -D
docker exec gnunet.n2.pep.example gnunet-namestore -z root -a -e "1 d" -p -t A -n n2 -V 172.19.0.4
docker exec gnunet.n2.pep.example gnunet-gns -t A -u n2.root
docker exec gnunet.n2.pep.example gnunet-namestore -z root -a -e "1 d" -p -t PKEY -n authority -V $AUTHORITY
docker exec gnunet.n2.pep.example gnunet-gns -t A -u authority.authority.root
docker exec authority.pep.example gnunet-namestore -z root -a -e "1 d" -p -t PKEY -n n2 -V $N2
docker exec authority.pep.example gnunet-gns -t A -u n2.n2.root

echo "In gnunet.n2, Is it possible now to ask about the n2 records that the authority have?"
# This does not answer, needs some time?
#docker exec gnunet.n2.pep.example gnunet-gns -t A -u n1.authority.root
# docker-compose stop
