Integration tests
=================

To run the integration test, you will need docker installed.

Mixnet test
-----------

Then run ``docker-compose up -d``

The test represent the following scenario:

Alice (A) want to send a message to Bob (B) routing it via N1, N2, N3.

For this test, the following docker containers are needed:

Alice
~~~~~

- has a keyring with the following keys: As, Ap, Bp, N3p, N2p, N1
- compose a message from Alice to Bob
- launch the mixnet client, the client will:

    1. encrypt the message for Bob
    2. encrypt the message for N3
    3. encrypt the message for N2
    4. encrypt the message for N1
    5. the final message is from A to N1
    6. send the message to N1

N1
~~

- has a kerying with the keys: N1s, N1p, N2p, N3p
- receives the message from A to N1
- the mixnet remailer is triggered, which:
  1. decrypts the message and obtain a message from A to N2
  2. change the from header to be from N1?
  3. send the message to N2

N2
~~

- has a kerying with the keys: N2s, N2p, N1p, N3p
- receives the message from A (or N1?) to N2
- the mixnet remailer is triggered, which:
  1. decrypts the message and obtain a message from A to N3
  2. change the from header to be from N2?
  3. send the message to N3

N3
~~

- has a kerying with the keys: N3s, N3p, N1p, N2p
- receives the message from A (or N2?) to N3
- the mixnet remailer is triggered, which:
  1. decrypts the message and obtain a message from A to B
  2. change the from header to be from N3?
  3. send the message to B

Bob
~~~

- has a kerying with the keys: Bs, Bp, Ap, N1p, N2p, N3p
- receives the message from A (or N3?) to B
- launch pep client, which:
  1. decrypts the message and obtain a message from A to B

GNUnet GNS tests
----------------

To run the tests that use the GNUnet cli, run ``docker/gnunet/test.sh``.
To run the tests that use the GNUnet REST API, run
``docker/gnunet/test_rest.sh``.
