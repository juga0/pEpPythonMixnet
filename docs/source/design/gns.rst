.. _gns:

Mixnet nodes registration in GNS
================================

.. Note: Any key mention of key here, refers to a public key.
   Also, we talk about two different keys: the mixnet nodes OpenPGP keys and
   their GNUnet GNS associated nodes keys.

See some documentation about GNS [gnunet-gns]_

V. proposed to use GNUnet GNS to register and retrieve mixnet nodes in the
mixnet network::

    Nodes should register, with an email address and a key, what is
    already in pEp identity. The key should be in ASCII armor Clients
    obtain the nodes from GNS.

GNUnet GNS limitations
----------------------

No TLS
~~~~~~

in the Web REST interface [rest-gns]_

Because it’s not possible to send/receive encrypted queries, every node
would need to run their own GNUnet node locally and send/receive queries
to it.

No authentication
~~~~~~~~~~~~~~~~~

in the Web REST interface [rest-gns]_

Not a global system
~~~~~~~~~~~~~~~~~~~

With a global name system like DNS, all the client would have the same
view of the network. An authority would still be needed for other
reasons.

Because GNS is not global, authority(s) are needed.

Delegating GNS records
~~~~~~~~~~~~~~~~~~~~~~

The authority(s) would neeed to add (to their ``gnunet-namestore``) the keys
of the mixnet GNS nodes to be able to resolve their records, ie. the
authority(s) delegates the resolution of the node records to the nodes.

Likewise, each mixnet GNS nodes would need to add the key of the authority too,
to be able to solve other GNS nodes, ie. each mixnet GNS node delegates the
resolution of other node records to the authority.

See [delegation]_ for more details.

In the following diagram, there's an authority and two nodes, showing which
records they'd store and which records they can query.
Note that 0000, 1111, 2222 would be the GNS nodes keys, while AAAA and
BBBB would be the mixnet nodes OpenPGP keys.

.. image:: /_static/mixnet_gns_components.svg

Node registration
~~~~~~~~~~~~~~~~~

The key(s) of the authority(s) would be hard-coded, so a mixnet GNS node can
easily add the authority(s) key(s) to its zone, to delegate to the authority the resolution of other nodes.

But the authority needs to get the (new) node key too. How this can be done?

* The node could add its own key querying the Web REST API of the authority,
  but the request would not be encrypted
* nkls is investigating using GNUnet cadet of file sharing

Node discovery by the client
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A mixnet client would need to know which are the mixnet nodes in the network.
It would need to also have a local GNS node to query records to the authority.

Since it is only possible to ask a record to the authority, but it is not
possible to ask all the records the authority knows about, the authority would
need to store in a record (called eg. mixnet) a list of all the mixnet (GNS)
nodes.
After retrieving that list, the client could ask the authority about a
node record.

A TXT record with the list of nodes could look like::

    n1,n2

Which means we can then ask the authority about ``n1`` and ``n2`` records.

GNS records
~~~~~~~~~~~

And which would be the records that the nodes should register?

Each node should register their Email address and their OpenPGP key.
There is no need to register the OpenPGP fingerprint, since it can be obtained
from the key, and it does not add any extra security to transmit the
fingerprint in the same "channel" the key is transmited.
They probably should also register the mixnet "layer" in which they'll operate.

CG proposed to use the CERT record type [cert]_, but it would only allow to
register the key, not the Email address and the layer.

We think TXT records are more suitable to store the triple.
It'd have the form::

    email=root@n1.pep.example;layer=1;opengpg=AAAA

TXT records are limited to 255 characters, and a OpenPGP key can be way
longer than that. But it's possible to add several records with the same name,
and they key can be splitted in several.
When querying the TXT record, the key can be reconstructed concatening all the
query results.

References:

.. [cert] https://git.gnunet.org/gnunet.git/tree/src/gnsrecord/plugin_gnsrecord_dns.c#n130
.. [gnunet-gns] https://gnunet.org/en/use.html#gns_cli
.. [rest-gns] https://rest.gnunet.org/
.. [delegation] https://docs.gnunet.org/handbook/gnunet.html#Adding-Links-to-Other-Zones
