.. pEp Python Remailer documentation master file, created by
   sphinx-quickstart on Thu Jan  2 15:06:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pEp Python Remailer's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:

   README
   integration
   design/*
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
