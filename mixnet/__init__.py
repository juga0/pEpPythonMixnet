from pkg_resources import DistributionNotFound, get_distribution

from .settings import settings  # noqa F401 imported but unused

try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    print("Package is not installed.")
