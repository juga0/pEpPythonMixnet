#! /usr/bin/env python3
"""remailer command line interface."""
import argparse
import email
import logging
import logging.config
import sys

import configargparse

from mixnet import __version__, common, defaults, exceptions
from mixnet.remailer import remailer
from mixnet.settings import settings


def create_parser():
    parser = configargparse.ArgParser(
        description="Send received Email to next Remailer or recipient.",
        default_config_files=[
            "/etc/mixnet.ini",
            "/etc/mixnet.d/*.ini",
            "~/.mixnet.ini",
        ],
    )
    parser.add("-c", "--config", is_config_file=True, help="Config file path.")
    parser.add_argument(
        "-d", "--debug", help="Set logging level to debug", action="store_true"
    )
    parser.add_argument(
        "--version",
        action="version",
        help="version",
        version="%(prog)s " + __version__,
    )
    parser.add_argument(
        "-a",
        "--home",
        help="This application home, where pEp management.db or GNUPGHOME "
        "live.",
    )
    parser.add_argument(
        "-g", "--gnupghome", help="The directory to set GNUPGHOME to."
    )
    parser.add_argument(
        "-k",
        "--keyring",
        type=argparse.FileType("r"),
        nargs="*",
        help="Import keys from a keyring path",
    )
    parser.add_argument(
        "-m",
        "--mynameaddress",
        help="Sender or this remailer name-address identity.",
    )
    parser.add_argument(
        "-p", "--myfpr", help="Sender or this remailer OpenPGP fingerprint."
    )
    parser.add_argument(
        "-f",
        "--email_path",
        type=argparse.FileType("r"),
        help="Email file path to forward to remailers. "
        "It can also be pass via STDIN.",
    )
    parser.add_argument(
        "-t",
        "--smtp",
        help="SMTP server to use to send an Email, in the form host[:port].",
        default="localhost:25",
    )
    parser.add_argument(
        "-n",
        "--test-net",
        help="Run in a test network/environment",
        action="store_true",
    )
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()
    if args.home:
        home = common.set_env(args.home, True)
    else:
        home = common.set_env()

    # Once the home is know, the log file can be stored there.
    defaults.LOGGING["handlers"]["file"]["filename"] = common.log_path(home)
    logging.config.dictConfig(defaults.LOGGING)
    logger = logging.getLogger(__name__)

    if args.debug:
        logger.setLevel(logging.DEBUG)
    logger.debug("Arguments: %s", args)

    if args.gnupghome:
        common.set_gnupg_home(args.gnupghome)

    if args.keyring:
        try:
            common.import_keys(args.keyring)
        except (exceptions.PepKeyNotFound, Exception) as e:
            logger.error(e)
            sys.exit(1)

    fd = None
    if args.email_path:
        fd = args.email_path
    else:
        fd = sys.stdin
    email_str = fd.read()
    logger.debug("Received Email:\n%s", email_str)

    mynameaddress = args.mynameaddress
    if not mynameaddress:
        email_msg = email.message_from_string(email_str)
        mynameaddress = email_msg.get("To", None)

    host, port = common.parse_host_port(args.smtp)

    if args.test_net:
        settings.TEST_NET = True

    try:
        remailer.process_email(
            email_str, mynameaddress, args.myfpr, host, port
        )
    except (
        exceptions.EmailNotDecrypted,
        exceptions.EmailNotTrusted,
        OSError,
    ) as e:
        logger.error(e)
        sys.exit(1)


if __name__ == "__main__":
    main()
