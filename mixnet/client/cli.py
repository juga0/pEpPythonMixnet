#! /usr/bin/env python3
"""client command line interface."""
import argparse
import email
import logging
import logging.config
import sys

import configargparse

from mixnet import __version__, common, defaults, exceptions
from mixnet.client import client
from mixnet.common import parse_host_port
from mixnet.settings import settings


def create_parser():
    parser = configargparse.ArgParser(
        description="Send Email to Remailers.",
        default_config_files=[
            "/etc/mixnet.ini",
            "/etc/mixnet.d/*.ini",
            "~/.mixnet.ini",
        ],
    )
    parser.add("-c", "--config", is_config_file=True, help="Config file path.")
    parser.add_argument(
        "-d", "--debug", help="Set logging level to debug", action="store_true"
    )
    parser.add_argument(
        "--version",
        action="version",
        help="version",
        version="%(prog)s " + __version__,
    )
    parser.add_argument(
        "-a",
        "--home",
        help="This application home, where pEp db or GNUPGHOME live.",
    )
    parser.add_argument(
        "-g", "--gnupghome", help="The directory to set GNUPGHOME to."
    )
    parser.add_argument(
        "-k",
        "--keyring",
        nargs="*",
        type=argparse.FileType("r"),
        help="Import keys from a keyring path",
    )
    # It is not needed to obtain the positional arguments sender and recpient
    # from the MTA because they're also inside the Email.
    parser.add_argument(
        "-r",
        "--recipient",
        help="Recipient email address."
        "It'll be ignored if email-path is provided.",
    )
    parser.add_argument(
        "-m",
        "--mynameaddress",
        help="Sender name-address or name-address to send the answer back."
        "It'll be ignored if email-path is provided.",
    )
    parser.add_argument(
        "-p", "--myfpr", help="Sender OpenPGP fingerprint.", default=""
    )
    parser.add_argument("-s", "--subject", help="Message subject.", default="")
    parser.add_argument("-b", "--body", help="Message body.", default="")
    parser.add_argument(
        "-f",
        "--email_path",
        type=argparse.FileType("r"),
        help="Email file path to forward to remailers."
        "It can also be pass via STDIN.",
    )
    parser.add_argument(
        "-t",
        "--smtp",
        help="SMTP server to use to send an Email, in the form host[:port].",
        default="localhost:25",
    )
    parser.add_argument(
        "-n",
        "--test-net",
        help="Run in a test network/environment",
        action="store_true",
    )
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    if args.home:
        home = common.set_env(args.home, True)
    else:
        home = common.set_env()

    # Once the home is know, the log file can be stored there.
    defaults.LOGGING["handlers"]["file"]["filename"] = common.log_path(home)
    logging.config.dictConfig(defaults.LOGGING)
    logger = logging.getLogger(__name__)

    if args.debug:
        logger.setLevel(logging.DEBUG)
    logger.debug("Arguments: %s", args)

    if args.gnupghome:
        common.set_gnupg_home(args.gnupghome)

    if args.keyring:
        try:
            common.import_keys(args.keyring)
        except (exceptions.PepKeyNotFound, Exception) as e:
            logger.error(e)
            sys.exit(1)

    sender = fd = recipient = body = subject = None
    if args.mynameaddress:
        sender = args.mynameaddress

    # email-path or stdin takes precedence over recipient
    if args.email_path:
        fd = args.email_path
    elif not args.recipient:
        fd = sys.stdin
    else:
        recipient = args.recipient
        body = args.body
        subject = args.subject
    if fd:
        email_str = fd.read()
        email_msg = email.message_from_string(email_str)
        # email_msg.set_content(email_str)
        sender = email_msg.get("From", None)
        recipient = email_msg.get("To", None)
        body = email_msg.get_payload()
        subject = email_msg.get("Subject", None)
    if not recipient:
        logger.error("No recipient. Provide an Email file or recipient arg.")
        sys.exit(1)

    host, port = parse_host_port(args.smtp)

    if args.test_net:
        settings.TEST_NET = True
    logger.info("Parsed arguments.")
    client.process(sender, recipient, args.myfpr, subject, body, host, port)


if __name__ == "__main__":
    main()
