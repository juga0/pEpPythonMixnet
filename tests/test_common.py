import pytest

from mixnet import common, exceptions


def test_import_keys_succeed(import_keys, alice_sec_key_path):
    common.import_keys([alice_sec_key_path])
    # XXX: How to verify the keys are in the pep keyring?>


def test_import_keys_not_imported(datadir):
    with pytest.raises(exceptions.PepNoKeyImported) as e:
        common.import_keys([datadir.basepath.join("nokey.asc")])
    assert str(e.value) == "No key imported."
