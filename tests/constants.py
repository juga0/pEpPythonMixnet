"""Constants for unit tests."""


ANGLE_ADDR = "<{}>"
NAME_ADDR = "{} {}"

BOB_NAME = "Bob Babagge"
BOB_ADDRESS = "bob@openpgp.example"
BOB_FP = "D1A66E1A23B182C9980F788CFBFCC82A015E7330"
BOB_NAME_ADDR = NAME_ADDR.format(BOB_NAME, ANGLE_ADDR.format(BOB_ADDRESS))

CAROL_NAME = "Carol Hopper"
CAROL_ADDRESS = "carol@openpgp.example"
CAROL_FP = "37D13DE1DCBAFCE7BCE117EE311399EB28E3E8AA"
CAROL_NAME_ADDR = NAME_ADDR.format(
    CAROL_NAME, ANGLE_ADDR.format(CAROL_ADDRESS)
)

ALICE_NAME = "Alice Lovelace"
ALICE_ADDRESS = "alice@openpgp.example"
ALICE_FP = "EB85BB5FA33A75E15E944E63F231550C4F47E38E"
ALICE_NAME_ADDR = NAME_ADDR.format(
    ALICE_NAME, ANGLE_ADDR.format(ALICE_ADDRESS)
)

SUBJECT_A_B = "Subject: from Alice to Bob"
BODY = "Hi Bob,\nthis is Alice!\n"
SUBJECT_B_C = "Subject: from Alice to Bob"
